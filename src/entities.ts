export interface Student{
    id:number;
    name:string;
    hired:boolean;
    promo:string;
}